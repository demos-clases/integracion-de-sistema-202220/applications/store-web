import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http: HttpClient) {}

  get(personId: string) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization':`Bearer ${token}`});

    return this.http.get(
      `http://localhost:8002/api/v1/person/information/${personId}`
      , {headers});
  }
}
