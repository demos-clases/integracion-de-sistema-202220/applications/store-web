import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {}

  login(email: string, password: string) {
    return this.http.post(`http://localhost:8002/api/v1/auth/login`, { email, password });
  }
}
