import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  error: string = '';
  token: string = '';
  user?: any;
  constructor(
    private loginService: LoginService
  ) { }


  ngOnInit(): void {
  }

  login(email: string, password: string) {
    console.log('do something with', email, password);
    this.loginService.login(email, password).subscribe(
      (data: any) => {
        this.token = data.token;
        this.user = data.user;
        localStorage.setItem('token', this.token);
        localStorage.setItem('user', JSON.stringify(this.user));
      },
      (error) => {
        this.error = "Error con el usuario o la contraseña";
      },
      () => {}
    );
  }

}
