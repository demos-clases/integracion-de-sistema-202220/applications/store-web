import { Component, OnInit } from '@angular/core';
import { PersonService } from '../services/person.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.sass']
})
export class PerfilComponent implements OnInit {

  constructor(
    private personService: PersonService,
  ) { }

  ngOnInit(): void {
    const jsonText = localStorage.getItem('user');
    const user = JSON.parse(jsonText!)?? null;
    this.personService.get(user.personId).subscribe((data) => {
      console.log(data);
    });
  }

}
