import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-store-main',
  templateUrl: './store-main.component.html',
  styleUrls: ['./store-main.component.sass']
})
export class StoreMainComponent implements OnInit {
  user?: any;
  constructor() { }

  ngOnInit(): void {
    const json = localStorage.getItem('user');
    if (json) {
      this.user = JSON.parse(json) || undefined;
    }
    console.log(this.user);
  }

}
