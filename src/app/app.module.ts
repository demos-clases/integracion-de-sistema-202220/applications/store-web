import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/screen/login/login.component';
import { RegisterComponent } from './auth/screen/register/register.component';
import { LogoutComponent } from './auth/screen/logout/logout.component';
import { LayoutComponent } from './auth/screen/layout/layout.component';
import { StoreLayoutComponent } from './store/screen/store-layout/store-layout.component';
import { StoreMainComponent } from './store/screen/store-main/store-main.component';
import { HttpClientModule } from '@angular/common/http';
import { PerfilComponent } from './auth/perfil/perfil.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    LayoutComponent,
    StoreLayoutComponent,
    StoreMainComponent,
    PerfilComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
