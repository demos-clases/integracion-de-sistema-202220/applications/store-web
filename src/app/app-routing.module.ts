import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PerfilComponent } from './auth/perfil/perfil.component';
import { LayoutComponent } from './auth/screen/layout/layout.component';
import { LoginComponent } from './auth/screen/login/login.component';
import { LogoutComponent } from './auth/screen/logout/logout.component';
import { RegisterComponent } from './auth/screen/register/register.component';
import { StoreLayoutComponent } from './store/screen/store-layout/store-layout.component';
import { StoreMainComponent } from './store/screen/store-main/store-main.component';

const routes: Routes = [
  { path: '', redirectTo: 'store', pathMatch: 'full' },
  {
    path: 'auth', component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'ingresar', pathMatch: 'full' },
      { path: 'perfil', component: PerfilComponent },
      { path: 'ingresar', component: LoginComponent },
      { path: 'salir', component: LogoutComponent },
      { path: 'registrarce', component: RegisterComponent },
    ]
  }, {
    path: 'store', component: StoreLayoutComponent,
    children: [
      { path: '', component: StoreMainComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
